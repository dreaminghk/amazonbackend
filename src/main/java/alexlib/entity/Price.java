package alexlib.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Price {

    private Date priceTime;
    private BigDecimal price;

    public Price(Date priceTime, BigDecimal price){
        this.priceTime = priceTime;
        this.price = price;
    }

    public Date getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Date priceTime) {
        this.priceTime = priceTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Price [price=" + price + ", priceTime=" + priceTime + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((priceTime == null) ? 0 : priceTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Price other = (Price) obj;
        if (price == null) {
            if (other.price != null)
                return false;
        } else if (!price.equals(other.price))
            return false;
        if (priceTime == null) {
            if (other.priceTime != null)
                return false;
        } else if (!priceTime.equals(other.priceTime))
            return false;
        return true;
    }

    
}
